const links = [
    {
        id: 1,
        text: 'Home',
        url: '#home'
    },
    {
        id: 2,
        text: 'About',
        url: '#about'
    },
    {
        id: 3,
        text: 'Skills',
        url: '#skills'
    },
    {
        id: 4,
        text: 'Portfolio',
        url: '#portfolio'
    },
    {
        id: 5,
        text: 'Contact',
        url: '#contact'
    }
]
export default links