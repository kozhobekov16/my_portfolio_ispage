const particlesConfig = {
  particles: {
    number: {
      density: {
        enable: true,
      },
    },
    color: {
      value: "#008ae6",
    },
    shape: {
      type: "circle",
      stroke: {
        width: 2,
        color: "#000000",
      },
      polygon: {
        nb_sides: 10,
      },
    },
    opacity: {
      value: 1,
      random: false,
      anim: {
        enable: false,
        speed: 1,
        opacity_min: 0.1,
        sync: false,
      },
    },
    size: {
      value: 2,
      random: true,
      anim: {
        enable: false,
        speed: 10000,
        size_min: 0.5,
        sync: false,
      },
    },
    line_linked: {
      enable: true,
      distance: 250,
      color: "#008ae6",
      opacity: 1,
      width: 0.7,
    },
    move: {
      enable: true,
      speed: 2,
      direction: "none",
      random: false,
      straight: false,
      out_mode: "out",
      bounce: false,
      attract: {
        enable: false,
        rotateX: 100,
        rotateY: 100,
      },
    },
  },
  interactivity: {
    detect_on: "canvas",
    events: {
      onhover: {
        enable: true,
        mode: "grab",
      },
      onclick: {
        enable: true,
        mode: "push",
      },
      resize: true,
    },
    modes: {
      grab: {
        distance: 100,
        line_linked: {
          opacity: 1,
        },
      },
      repulse: {
        distance: 10,
        duration: 0.6,
      },
      push: {
        particles_nb: 4,
      },
      remove: {
        particles_nb: 2,
      },
    },
  },
  // particles: {
  //   number: {
  //     value: 50,
  //   },
  //   size: {
  //     value: 3,
  //   },
  // },
  // interactivity: {
  //   events: {
  //     onhover: {
  //       enable: true,
  //       mode: "repulse",
  //     },
  //   },
  // },
  retina_detect: true,
};

export default particlesConfig;
