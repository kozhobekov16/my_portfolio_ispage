import React from "react";
import "./App.scss";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import { useEffect, useState } from "react";
import ParticleBackground from "./Particle/ParticleBackground";
//components
import CenterTitle from "./components/CenterTitle/CenterTitle";
import About from './components/About/About'
import Portfolio from "./components/Portfolio/Portfolio";
import { Skills } from "./components/Skills/Skills";
import { Footer } from "./components/Footer/Footer";
import ClimbingBoxLoader from 'react-spinners/ClimbingBoxLoader'
function App() {
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setLoading(true);
  }, []);
  return (
    <Router>
      <Switch>
        <div>
          {loading === false ? (
            <div className="ringLoader">
              {" "}
              <ClimbingBoxLoader className="loader" />
            </div>
          ) : (
            <div className="App" id="home">
              <ParticleBackground />
              <CenterTitle />
              <div id="about">
                <About />
              </div>
              <div id="skills">
                <Skills />
              </div>
              <div id="portfolio">
                <Portfolio />
              </div>
              <div  id="contacts">
                <Footer />
              </div>
            </div>
          )}
        </div>
      </Switch>
    </Router>
  );
}

export default App;
