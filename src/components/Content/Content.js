import React from "react";
import "./Content.scss";
import me from "../../assets/me.png";
const Content = (props) => {
  let items = props.elems.map((i) => <li>{i.title}</li>);
  return (
    <section>
      <div class="container">
        <div class="text-container">
          <div className="me">
            <img src={me} className="me" />
          </div>
          <p class="static-text">I'm a </p>
          <ul class="dynamic-text">
            <li className="item">{items}</li>
          </ul>
        </div>
      </div>
    </section>
  );
};
export default Content;
