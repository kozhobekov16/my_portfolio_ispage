import React from "react";
// import aboutImage from '../../assets/photo_2021-07-26_14-23-01.jpg'
// import aboutImage from "../../assets/photo_2021-07-26_14-23-11.jpg";
// import aboutImage from "../../assets/blackw.JPG";
// import aboutImage from "../../assets/white.JPG";
import classes from "./About.module.scss";
const About = () => {
  return (
    <section className={classes.aboutMe}>
      {/* <div>
        <img className={classes.aboutImage} src={aboutImage} alt="me"/>
      </div> */}
      <div className={classes.aboutText}>
        <h2 className={classes.name}>Kozhobekov Zhakshylyk</h2>
        <h2 className={classes.skill}>Creative Independent Web Developer based in Kyrgyzstan</h2>
        <p>
          I'm web developer, and I'm very passionate and dedicated to my work.
          With 1,5 years experience as a professional web developer, I have
          acquired the skills and knowledge necessary to make your project a
          success. I enjoy every step of the development process, from
          discussion and collaboration.
        </p>
        {/* <p>
          E-mail:{" "}
          <b>
            <i>zhakshylykkozhobekov@gmail.com</i>
          </b>
        </p> */}
        <p>
          More about me:{" "}
        </p>
        {/* <button className={classes.btn}>See portfolio</button> */}
      </div>
    </section>
  );
};
export default About;
