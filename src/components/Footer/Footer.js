import React from 'react'
import './Footer.scss'
export const Footer = () => {
    return (
            <section className="footer">
                © 2021 Zhakshylyk Kozhobekov
            </section>
    )
}
