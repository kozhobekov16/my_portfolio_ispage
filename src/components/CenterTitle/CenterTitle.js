import React from "react";
import Header from "../Header/Header";
import Content from "../Content/Content";
import { BrowserRouter } from "react-router-dom";
function CenterTitle() {
  let elems = [
    { title: "Web developer" },
    { title: "Mobile developer" },
    { title: "Content creator" },
  ];
  return (
    <BrowserRouter>
      <div id="text_div center_all">
        <div className="center_all">
          {" "}
          <Header />
          <Content elems={elems} />
        </div>
      </div>
    </BrowserRouter>
  );
}

export default CenterTitle;
