import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import classes from "./Header.module.scss";
import links from '../../datas/links'
const Header = (props) => {
  const [click, setClick] = useState(false)
  const [navbar, setNavbar] = useState(false)

  const handleClick = () => {
    setClick(!click)
  }

  const changeBack = () => {
    if (window.scrollY >= 80) {
      setNavbar(true)
    } else {
      setNavbar(false)
    }
  }

  window.addEventListener('scroll', changeBack)
  return (
    <section className={navbar ? classes.activeMenu : classes.header}>
      <NavLink to="#home" className={classes.logo}>
        Zhakshylyk
      </NavLink>
      <div className={classes.links}>
        <div className={click ? classes.activeBurger : classes.menus}>
          {links.map(links => {
            return (
              <a href={links.url} activeClassName={classes.active} key={links.id}>
                {links.text}
              </a>
            )
          })}
          {/* <NavLink to="about" activeClassName={classes.active}>
            About
          </NavLink>
          <NavLink to="portfolio" activeClassName={classes.active}>
            Portfolio
          </NavLink>
          <NavLink to="contact" activeClassName={classes.active}>
            Contact
          </NavLink> */}
        </div>
      </div>
      <div className={click ? classes.socialsActive : classes.socials}>
        <a
          target="_blank"
          className="linkedin"
          href="https://www.linkedin.com/in/zhakshylyk-kozhobekov-3b4b02219/"
        >
          {" "}
          <i class="fab fa-linkedin"></i>
        </a>
        <a target="_blank" href="https://www.instagram.com/?hl=ru">
          {" "}
          <i class="fab fa-instagram"></i>
        </a>
        <a target="_blank" href="https://t.me/kozhobekov_16">
          {" "}
          <i class="fab fa-telegram"></i>
        </a>
      </div>
      <div className={classes.burger} onClick={handleClick}>
        <i className={click ? "far fa-window-close" : "fas fa-bars"}></i>
      </div>
    </section>
  );
};
export default Header;
