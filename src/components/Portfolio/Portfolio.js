import React from "react";
import "./Portfolio.scss";
// photo newknau
import newKnauMain from "../../assets/portfolioPhotos/Снимок экрана от 2021-07-26 22-35-29.png";
import oneNew from "../../assets/portfolioPhotos/Снимок экрана от 2021-07-26 22-45-56.png";
import twoNew from "../../assets/portfolioPhotos/Снимок экрана от 2021-07-26 22-46-21.png";
import threeNew from "../../assets/portfolioPhotos/Снимок экрана от 2021-07-26 22-46-40.png";
// armada
import armadaMain from "../../assets/armada/Снимок экрана от 2021-07-26 23-14-22.png";
import armada1 from "../../assets/armada/Снимок экрана от 2021-07-26 23-14-29.png";
import armada2 from "../../assets/armada/Снимок экрана от 2021-07-26 23-15-47.png";
import armada3 from "../../assets/armada/Снимок экрана от 2021-07-26 23-16-18.png";
import armada4 from "../../assets/armada/Снимок экрана от 2021-07-26 23-16-24.png";
// agrodev
import mainAgro from "../../assets/portfolioPhotos/agrodev/Снимок экрана от 2021-07-26 22-55-21.png";
import oneAgro from "../../assets/portfolioPhotos/agrodev/Снимок экрана от 2021-07-26 22-55-27.png";
import twoAgro from "../../assets/portfolioPhotos/agrodev/Снимок экрана от 2021-07-26 22-55-30.png";
import threeAgro from "../../assets/portfolioPhotos/agrodev/Снимок экрана от 2021-07-26 22-55-41.png";
import fourAgro from "../../assets/portfolioPhotos/agrodev/Снимок экрана от 2021-07-26 22-55-54.png";
//iisido
import isidoMain from "../../assets/iisido/Снимок экрана от 2021-07-26 23-27-03.png";
import oneI from "../../assets/iisido/Снимок экрана от 2021-07-26 23-27-07.png";
import twoI from "../../assets/iisido/Снимок экрана от 2021-07-26 23-27-10.png";
import threeI from "../../assets/iisido/Снимок экрана от 2021-07-26 23-27-13.png";
import fourI from "../../assets/iisido/Снимок экрана от 2021-07-26 23-27-38.png";
//ithub 
import hub1 from '../../assets/ithub/Снимок экрана от 2021-07-28 14-27-38.png'
import hub2 from '../../assets/ithub/Снимок экрана от 2021-07-28 14-27-44.png'
import hub3 from '../../assets/ithub/Снимок экрана от 2021-07-28 14-27-51.png'
import hub4 from '../../assets/ithub/Снимок экрана от 2021-07-28 14-28-09.png'
//feim 
import feim1 from '../../assets/feim/Снимок экрана от 2021-07-28 14-33-28.png'
import feim2 from '../../assets/feim/Снимок экрана от 2021-07-28 14-33-52.png'
import feim3 from '../../assets/feim/Снимок экрана от 2021-07-28 14-36-21.png'
import feim4 from '../../assets/feim/Снимок экрана от 2021-07-28 14-36-25.png'
import feim5 from '../../assets/feim/Снимок экрана от 2021-07-28 14-36-29.png'
//sbbar
import sb1 from '../../assets/sbbar/Снимок экрана от 2021-07-28 14-40-43.png'
import sb2 from '../../assets/sbbar/Снимок экрана от 2021-07-28 14-40-45.png'
import sb3 from '../../assets/sbbar/Снимок экрана от 2021-07-28 14-40-48.png'
import sb4 from '../../assets/sbbar/Снимок экрана от 2021-07-28 14-40-51.png'
//vestnik
import ves1 from '../../assets/vestnik/Снимок экрана от 2021-07-28 14-44-54.png'
import ves2 from '../../assets/vestnik/Снимок экрана от 2021-07-28 14-44-59.png'
import ves3 from '../../assets/vestnik/Снимок экрана от 2021-07-28 14-45-02.png'
//express24
import ex from '../../assets/express24/Снимок экрана 2021-10-19 163006.png'
import ex1 from '../../assets/express24/Снимок экрана 2021-10-19 163057.png'
import ex3 from '../../assets/express24/Снимок экрана 2021-10-19 163114.png'
//itf
import itf from '../../assets/itf/Снимок экрана от 2021-07-28 14-49-46.png'
//fvm
import fvm from '../../assets/fvm/Снимок экрана от 2021-07-28 14-52-14.png'
//valcourse
import val from '../../assets/valcourse/Снимок экрана 2021-10-20 135752.png'
import val2 from '../../assets/valcourse/Снимок экрана 2021-10-20 135822.png'
//bono
import bono from '../../assets/bono/b.png'
import bono2 from '../../assets/bono/Снимок экрана 2021-10-22 142806.png'
import bono3 from '../../assets/bono/Снимок экрана 2021-10-22 142827.png'
//gruzovik
import gruz from '../../assets/gruzovik/1.png'
import gruz2 from '../../assets/gruzovik/2.png'
import gruz3 from '../../assets/gruzovik/3.png'
import gruz4 from '../../assets/gruzovik/4.png'
//cdo
import cdo from '../../assets/cdo/1.png'
import cdo2 from '../../assets/cdo/2.png'

const Portfolio = () => {
  return (
    <div>
      <p className="title">My portfolio</p>
      <p className="titleTwo">I offer modern websites and apps</p>
      <div id="container">
        <div className="items">
          <div className="content">
            <a href="https://knau.kg" target="_blank" rel="noreferrer">
              knau.kg
            </a>
            <img src={newKnauMain} alt="imag" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={oneNew} alt="img" />
            </div>
            <div className="preview-image">
              <img src={newKnauMain} alt="img" />
            </div>
            <div className="preview-image">
              <img src={twoNew} alt="img" />
            </div>
            <div className="preview-image">
              <img src={threeNew} alt="img" />
            </div>
            <div className="preview-image">
              <img src={twoNew} alt="img" />
            </div>
          </div>
        </div>
        <div className="items">
          <div className="content">
            <a href="https://cdo.knau.kg" target="_blank" rel="noreferrer">
              cdo.knau.kg
            </a>
            <img src={cdo} alt="imag" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={cdo} alt="imag" />
            </div>
            <div className="preview-image">
              <img src={cdo2} alt="imag" />
            </div>
            <div className="preview-image">
              <img src={cdo2} alt="imag" />
            </div>
            <div className="preview-image">
              <img src={cdo} alt="imag" />
            </div>
            <div className="preview-image">
              <img src={cdo2} alt="imag" />
            </div>
          </div>
        </div>
        <div className="items">
          <div className="content">
            <a href="https://bono.wpshop.tech/" target="_blank" rel="noreferrer">
              bono.wpshop.tech
            </a>
            <img src={bono} alt="img" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={bono2} alt="img" />
            </div>
            <div className="preview-image">
              <img src={bono3} alt="img" />
            </div>
            <div className="preview-image">
              <img src={bono} alt="img" />
            </div>
            <div className="preview-image">
              <img src={bono3} alt="img" />
            </div>
            <div className="preview-image">
              <img src={bono2} alt="img" />
            </div>
          </div>
        </div>
        <div className="items">
          <div className="content">
            <a href="https://iisido.knau.kg" target="_blank" rel="noreferrer">
              iisido.knau.kg
            </a>
            <img src={isidoMain} alt="img" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={oneI} alt="img" />
            </div>
            <div className="preview-image">
              <img src={twoI} alt="img" />
            </div>
            <div className="preview-image">
              <img src={threeI} alt="img" />
            </div>
            <div className="preview-image">
              <img src={fourI} alt="img" />
            </div>
            <div className="preview-image">
              <img src={oneI} alt="img" />
            </div>
          </div>
        </div>
      </div>
      <div id="container">
        <div className="items">
          <div className="content">
            <a href="https://ithub.kg" target="_blank" rel="noreferrer">
              ithub.kg
            </a>
            <img src={hub1} alt="img" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={hub2} alt="img" />
            </div>
            <div className="preview-image">
              <img src={hub3} alt="img" />
            </div>
            <div className="preview-image">
              <img src={hub4} alt="img" />
            </div>
            <div className="preview-image">
              <img src={hub1} alt="img" />
            </div>
            <div className="preview-image">
              <img src={hub2} alt="img" />
            </div>
          </div>
        </div>
        <div className="items">
          <div className="content">
            <a href="https://feim.knau.kg" target="_blank" rel="noreferrer">
              feim.knau.kg
            </a>
            <img src={feim1} alt="img" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={feim2} alt="img" />
            </div>
            <div className="preview-image">
              <img src={feim3} alt="img" />
            </div>
            <div className="preview-image">
              <img src={feim4} alt="img" />
            </div>
            <div className="preview-image">
              <img src={feim5} alt="img" />
            </div>
            <div className="preview-image">
              <img src={feim1} alt="img" />
            </div>
          </div>
        </div>
        <div className="items">
          <div className="content">
            <a href="https://sbbar.kg" target="_blank" rel="noreferrer">
              sbbar.kg
            </a>
            <img src={sb2} alt="ima" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={sb1} alt="ima" />
            </div>
            <div className="preview-image">
              <img src={sb3} alt="ima" />
            </div>
            <div className="preview-image">
              <img src={sb4} alt="ima" />
            </div>
            <div className="preview-image">
              <img src={sb1} alt="ima" />
            </div>
            <div className="preview-image">
              <img src={sb3} alt="ima" />
            </div>
          </div>
        </div>
        <div className="items">
          <div className="content">
            <a href="https://vestnik.knau.kg" target="_blank" rel="noreferrer">
              vestnik.knau.kg
            </a>
            <img src={ves1} alt="ima" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={ves2} alt="ima" />
            </div>
            <div className="preview-image">
              <img src={ves1} alt="ima" />
            </div>
            <div className="preview-image">
              <img src={ves3} alt="ima" />
            </div>
            <div className="preview-image">
              <img src={ves2} alt="ima" />
            </div>
            <div className="preview-image">
              <img src={ves1} alt="ima" />
            </div>
          </div>
        </div>
      </div>
      <div id="container">
        <div className="items">
          <div className="content">
            <a href="https://itf.knau.kg" target="_blank" rel="noreferrer">
              itf.knau.kg
            </a>
            <img src={itf} alt="ima" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={itf} alt="ima" />
            </div>
            <div className="preview-image">
              <img src={itf} alt="ima" />
            </div>
            <div className="preview-image">
              <img src={itf} alt="ima" />
            </div>
            <div className="preview-image">
              <img src={itf} alt="ima" />
            </div>
            <div className="preview-image">
              <img src={itf} alt="ima" />
            </div>
          </div>
        </div>
        <div className="items">
          <div className="content">
            <a href="https://agrodev.knau.kg" target="_blank" rel="noreferrer">
              agrodev.knau.kg
            </a>
            <img src={mainAgro} alt="img" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={twoAgro} alt="img" />
            </div>
            <div className="preview-image">
              <img src={threeAgro} alt="img" />
            </div>
            <div className="preview-image">
              <img src={fourAgro} alt="img" />
            </div>
            <div className="preview-image">
              <img src={oneAgro} alt="img" />
            </div>
            <div className="preview-image">
              <img src={twoAgro} alt="img" />
            </div>
          </div>
        </div>
        <div className="items">
          <div className="content">
            <a href="https://armada.kg" target="_blank" rel="noreferrer">
              armada.kg
            </a>
            <img src={armadaMain} alt="img" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={armada1} alt="img" />
            </div>
            <div className="preview-image">
              <img src={armada2} alt="img" />
            </div>
            <div className="preview-image">
              <img src={armada3} alt="img" />
            </div>
            <div className="preview-image">
              <img src={armada4} alt="img" />
            </div>
            <div className="preview-image">
              <img src={armada1} alt="img" />
            </div>
          </div>
        </div>
        <div className="items">
          <div className="content">
            <a href="http://fvm.knau.kg/ru/" target="_blank" rel="noreferrer">
              fvm.knau.kg
            </a>
            <img src={fvm} alt="2" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={fvm} alt="2" />
            </div>
            <div className="preview-image">
              <img src={fvm} alt="2" />
            </div>
            <div className="preview-image">
              <img src={fvm} alt="2" />
            </div>
            <div className="preview-image">
              <img src={fvm} alt="2" />
            </div>
            <div className="preview-image">
              <img src={fvm} alt="2" />
            </div>
          </div>
        </div>
      </div>
      <div id="container">
        <div className="items">
          <div className="content">
            <a href="https://valcourse.com/" target="_blank" rel="noreferrer">
              valcourse.com
            </a>
            <img src={val} alt="2" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={val2} alt="2" />
            </div>
            <div className="preview-image">
              <img src={val} alt="2" />
            </div>
            <div className="preview-image">
              <img src={val2} alt="2" />
            </div>
            <div className="preview-image">
              <img src={val} alt="2" />
            </div>
            <div className="preview-image">
              <img src={val2} alt="2" />
            </div>
          </div>
        </div>
        <div className="items">
          <div className="content">
            <a href="https://gruzovik.com.kg" target="_blank" rel="noreferrer">
              gruzovik.com.kg
            </a>
            <img src={gruz} alt="2" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={gruz4} alt="2" />
            </div>
            <div className="preview-image">
              <img src={gruz2} alt="2" />
            </div>
            <div className="preview-image">
              <img src={gruz3} alt="2" />
            </div>
            <div className="preview-image">
              <img src={gruz4} alt="2" />
            </div>
            <div className="preview-image">
              <img src={val2} alt="2" />
            </div>
          </div>
        </div>
        <div className="items">
          <div className="content">
            <a href="https://express24.kg" target="_blank" rel="noreferrer">
              express24.kg
            </a>
            <img src={ex} alt="imag" />
          </div>
          <div className="previews">
            <div className="preview-image">
              <img src={ex} alt="imag" />
            </div>
            <div className="preview-image">
              <img src={ex1} alt="imag" />
            </div>
            <div className="preview-image">
              <img src={ex3} alt="imag" />
            </div>
            <div className="preview-image">
              <img src={ex} alt="imag" />
            </div>
            <div className="preview-image">
              <img src={ex1} alt="imag" />
            </div>
          </div>
        </div>
        <div className="items">
        </div>
      </div>
    </div>
  );
};
export default Portfolio;
