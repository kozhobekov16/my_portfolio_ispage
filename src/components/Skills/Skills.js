import React from "react";
import "./Skills.scss";
export const Skills = () => {
  return (
    <div className="skill">
      <h2 className="skill-title">
        I have high skills in developing and programming
      </h2>
      <div class="skills-bar">
        <div class="bar">
          <div class="info">
            <span>
              HTML/CSS (sass, less, Tailwind, Material UI, Bootstrap, MaterializeCss) FIGMA, PHOTOSHOP,
              ADOBE XD, AVOCODE
            </span>
          </div>
          <div class="progress-line html">
            <span></span>
          </div>
        </div>

        <div class="bar">
          <div class="info">
            <span>JAVASCRIPT es6+ (React (react-router-dom, hooks, Redux), Basic Vue3, TypeScript, jQuery, React Native)</span>
          </div>
          <div class="progress-line javascript">
            <span></span>
          </div>
        </div>

        <div class="bar">
          <div class="info">
            <span>
              {" "}
              REST API (Fetch, Axios), AJAX, GIT, NPM, YARN, LINUX, WEBPACK (PLUGINS)
            </span>
          </div>
          <div class="progress-line php">
            <span></span>
          </div>
        </div>

        <div class="bar">
          <div class="info">
            <span>Java (EE), Ruby</span>
          </div>
          <div class="progress-line python">
            <span></span>
          </div>
        </div>

        <div class="bar">
          <div class="info">
            <span>
              CMS (Wordpress (Elementor, PageBuilder, Woocomerce, PHP, SMTP) , Joomla),
            </span>
          </div>
          <div class="progress-line mysql">
            <span></span>
          </div>
        </div>
      </div>
    </div>
  );
};
